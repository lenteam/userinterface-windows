﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace AI_win
{
    
    public partial class MainWindow : Window
    {
        ColmenService.ColMenService_SOAPSoapClient client = new ColmenService.ColMenService_SOAPSoapClient("ColMenService_SOAPSoap");
        Dictionary<int, string> UserList = new Dictionary<int, string>();
        Dictionary<int, string> PersonList = new Dictionary<int, string>();
        Dictionary<int, string> KeywordList = new Dictionary<int, string>();
        Dictionary<int, string> SiteList = new Dictionary<int, string>();


        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.WidthAndHeight;
        }
        private void Autorisation(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            if (client.AutoriseUser(userBox.Text, passBox.Password) == 2) tabControl.IsEnabled = true;
            (sender as Button).IsEnabled = true;
        }

        private void Kill_Autorisation(object sender, RoutedEventArgs e)
        {
            tabControl.IsEnabled = false;
        }

        private void GetUserList(object sender, RoutedEventArgs e)
        {
            ItemsForComboBox();
        }

        private void ItemsForComboBox()
        {
            var dt = client.AdminGetUserList(userBox.Text, passBox.Password);
            UserList.Clear();
            foreach (DataRow row in dt.Rows) UserList.Add(int.Parse(row[0].ToString()), row[1].ToString());
            comboBox.ItemsSource = UserList.Values.ToList();
        }

        private void DeleteUser(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBox.Show(client.AdminDeleteUser(userBox.Text, passBox.Password, UserList.Where(x => x.Value == comboBox.SelectedItem.ToString()).FirstOrDefault().Key));
            }
            catch { MessageBox.Show("Что-то пошло не так"); }
            finally { ItemsForComboBox();}
        }

        private void AddNewUser(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NewUser.Text.Length > 2 && NewUser.Text.Length < 20 && NewPass.Text.Length > 2 && NewPass.Text.Length < 20)
                {
                    if (IsAdmin.IsChecked == true)
                        client.AdminAddNewAdmin(userBox.Text, passBox.Password, NewUser.Text, NewPass.Text);
                    else
                        client.AdminAddNewUser(userBox.Text, passBox.Password, NewUser.Text, NewPass.Text);
                }
                ItemsForComboBox();
            }
            catch { MessageBox.Show("Что-то пошло не так"); }
        }

        
        private void GetPersonList(object sender, RoutedEventArgs e)
        {
            GetPersonsForBox();
        }

        private void GetPersonsForBox()
        {
            var dt = client.GetPersons(userBox.Text, passBox.Password);
            PersonList.Clear();
            foreach (DataRow row in dt.Rows) PersonList.Add(int.Parse(row[0].ToString()), row[1].ToString());
            PersonsBox.ItemsSource = PersonList.Values.ToList();
        }

        private void GetKeywordList(object sender, RoutedEventArgs e)
        {
            GetKeywordsForBox();
        }

        private void GetKeywordsForBox()
        {
            var dt = client.GetKeyWordsList(userBox.Text, passBox.Password);
            KeywordList.Clear();
            foreach (DataRow row in dt.Rows) KeywordList.Add(int.Parse(row[0].ToString()), row[1].ToString());
            KeywordBox.ItemsSource = KeywordList.Values.ToList();
        }

        private void GetSiteList(object sender, RoutedEventArgs e)
        {
            GetSitesForBox();
        }

        private void GetSitesForBox()
        {
            var dt = client.GetSites(userBox.Text, passBox.Password);
            SiteList.Clear();
            foreach (DataRow row in dt.Rows) SiteList.Add(int.Parse(row[0].ToString()), row[1].ToString());
            SiteBox.ItemsSource = SiteList.Values.ToList();
        }

        private void DeletePersonButton_Click(object sender, RoutedEventArgs e)
        {
            if (PersonsBox.Text != null)
            {
                MessageBox.Show(client.AdminDeletePerson(userBox.Text, passBox.Password, PersonList.Where(x => x.Value == PersonsBox.SelectedItem.ToString()).FirstOrDefault().Key));
                GetPersonsForBox();
            }
                
        }

        private void DeleteKeywordButton_Click(object sender, RoutedEventArgs e)
        {
            if (KeywordBox.Text != null)
            {
                MessageBox.Show(client.AdminDeleteKeyWord(userBox.Text, passBox.Password, KeywordList.Where(x => x.Value == KeywordBox.SelectedItem.ToString()).FirstOrDefault().Key));
                GetKeywordsForBox();
            }
        }

        private void DeleteSiteButton_Click(object sender, RoutedEventArgs e)
        {
            if (SiteBox.Text != null)
            {
                MessageBox.Show(client.AdminDeleteSite(userBox.Text, passBox.Password, SiteList.Where(x => x.Value == SiteBox.SelectedItem.ToString()).FirstOrDefault().Key));
                GetSitesForBox();
            }
        }

        private void AddKeywordButton_Click(object sender, RoutedEventArgs e)
        {
            if (keywordTb.Text.Length > 1 && PersonsBox.SelectedItem!=null)
            {
                MessageBox.Show(client.AdminAddNewKeyWord(userBox.Text, passBox.Password, keywordTb.Text, PersonList.Where(x=>x.Value==PersonsBox.SelectedItem.ToString()).FirstOrDefault().Key));
                GetKeywordsForBox();
            }
        }

        private void AddPersonButton_Click(object sender, RoutedEventArgs e)
        {
            if (personTb.Text.Length > 1)
            {
                MessageBox.Show(client.AdminAddNewPerson(userBox.Text, passBox.Password, personTb.Text));
                GetPersonsForBox();
            }
        }

        private void AddSiteButton_Click(object sender, RoutedEventArgs e)
        {
            if(siteTb.Text.Length>3)
            {
                MessageBox.Show(client.AdminAddNewSite(userBox.Text, passBox.Password, siteTb.Text));
                GetSitesForBox();
            }
            
        }
    }
}
