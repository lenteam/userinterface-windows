﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ColMenSite
{

    [WebService(Namespace = "http://lenteam.hldns.ru")]
    public class ColMenService_SOAP : WebService
    {

        MySqlConnection con = new MySqlConnection("server=localhost;user id=colmen;persistsecurityinfo=True;database=colmen;allowuservariables=True;CharSet=utf8;");

        [WebMethod(Description ="Получение списка обрабатываемых персон, формат DataTable")]
        public DataTable GetPersons(string user, string password)
        {
            ConnectionStateChecker();
            DataTable dt = new DataTable();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password),con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return dt;
                }
            }
            MySqlCommand com = new MySqlCommand(@"SELECT * FROM colmen.Persons;", con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }

        [WebMethod(Description = "Получение списка обрабатываемых сайтов, формат DataTable")]
        public DataTable GetSites(string user, string password)
        {
            ConnectionStateChecker();
            DataTable dt = new DataTable();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password),con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return dt;
                }
            }
            MySqlCommand com = new MySqlCommand(@"SELECT * FROM colmen.Sites;", con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }

                
        [WebMethod(Description = "Получение URL страницы по её ID, формат string")]
        public string GetOnePageFromID(string user, string password, int id)
        {
            ConnectionStateChecker();
            string dt = "error";

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password),con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return dt;
                }
            }

            MySqlCommand com = new MySqlCommand(string.Format(@"SELECT* FROM Pages WHERE ID = '{0}';", id), con);
            using (var reader = com.ExecuteReader())
            {

                try
                {
                    while (reader.Read())
                    {
                        dt = reader[1].ToString();
                    }
                }
                catch { }

            }
            return dt;
        }


        [WebMethod(Description = "Добавление нового пользователя, входные параметры: имя пользователя,который создает новый аккаунт, его пароль, имя нового пользователя, пароль нового пользователя")]
        public string AdminAddNewUser(string user, string password, string newuser, string newuserspassword)
        {
            ConnectionStateChecker();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }

            MySqlCommand com = new MySqlCommand(string.Format(@"INSERT INTO UserData (UserLogin, UserPassword, isAdmin, IsActive) VALUES ('{0}', '{1}', false, true)", newuser, newuserspassword), con);
            com.ExecuteNonQuery();
            return "new user added!";
        }

        [WebMethod(Description = "Добавление нового администратора, входные параметры: имя пользователя,который создает новый аккаунт, его пароль, имя нового администратора, пароль нового администратора")]
        public string AdminAddNewAdmin(string user, string password, string newuser, string newuserspassword)
        {
            ConnectionStateChecker();

            
            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }

            MySqlCommand com = new MySqlCommand(string.Format(@"INSERT INTO UserData (UserLogin, UserPassword, isAdmin, IsActive) VALUES ('{0}', '{1}', true, true)", newuser, newuserspassword), con);
            com.ExecuteNonQuery();
            return "new user added!";
        }


        [WebMethod(Description = "Получить общую статистику для всех персон по сайту, формат DataTable")]
        public DataTable GetAllPersonStatistic(string user, string password)
        {
            ConnectionStateChecker();


            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return new DataTable();
                }
            }

            DataTable dt = new DataTable("Persons");
            MySqlCommand com = new MySqlCommand(@"select PersonID,Name,sum(Rank) from Pages,PersonPageRank,Persons where Persons.ID = PersonID and Pages.ID=PersonPageRank.PageID group by PersonID;", con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }

        [WebMethod(Description = "Получить информацию по отдельной персоне, по отдельному сайту, за определенную дату, формат DataTable")]
        public DataTable GetSinglePersonInfoPerSitePerDate(string user, string password, int PersonID, int SiteId, DateTime date)
        {
            ConnectionStateChecker();


            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return new DataTable();
                }
            }

            DataTable dt = new DataTable("Persons");
            MySqlCommand com = new MySqlCommand(string.Format(@"select P.Url,P.LastScanDate,R.Rank from PersonPageRank R,Pages P where R.PageID = P.ID and PersonID='{0}' and SiteID='{1}' and LastScanDate='{3}'", PersonID,SiteId,date.ToString("yyyy-MM-dd")), con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }

        [WebMethod(Description = "Получить информацию по отдельной персоне, по отдельному сайту, за последние 30 дней, формат DataTable")]
        public DataTable GetSinglePersonInfoPerSite(string user, string password, int PersonID, int SiteId)
        {
            ConnectionStateChecker();


            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return new DataTable();
                }
            }

            DataTable dt = new DataTable("Persons");
            MySqlCommand com = new MySqlCommand(string.Format(@"select P.Url,P.LastScanDate,R.Rank from PersonPageRank R,Pages P where R.PageID = P.ID and PersonID='{0}' and SiteID='{1}' and LastScanDate>='{2}'", PersonID, SiteId, DateTime.Now.AddDays(-30).ToString("yyyy-MM-dd")), con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }


        [WebMethod(Description = "Получить список ключевых слов, формат DataTable")]
        public DataTable GetKeyWordsList(string user, string password)
        {
            ConnectionStateChecker();


            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return null;
                }
            }

            DataTable dt = new DataTable();
            MySqlCommand com = new MySqlCommand(@"SELECT * FROM colmen.Keywords;", con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;

            
        }


        [WebMethod(Description = "Добавление нового сайта")]
        public string AdminAddNewSite(string user, string password, string name)
        {
            ConnectionStateChecker();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }
            UriBuilder ubuilder = new UriBuilder("http", name);
            MySqlCommand com = new MySqlCommand(string.Format(@"INSERT INTO Sites (Name) VALUES ('{0}')", ubuilder.ToString()), con);
            com.ExecuteNonQuery();
            return "new site added!";
        }

        [WebMethod(Description = "Удаление сайта из обработки")]
        public string AdminDeleteSite(string user, string password, int SiteID)
        {
            ConnectionStateChecker();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }
            
            MySqlCommand com = new MySqlCommand(string.Format(@"delete from Sites Where ID ={0}", SiteID), con);
            com.ExecuteNonQuery();
            return "Site Deleted!";
        }


        [WebMethod(Description = "Получить список пользователей")]
        public DataTable AdminGetUserList(string user, string password)
        {
            ConnectionStateChecker();
            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return null;
                }
            }

            DataTable dt = new DataTable();
            MySqlCommand com = new MySqlCommand(@"SELECT ID, UserLogin, IsAdmin FROM colmen.UserData;", con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;

        }

        [WebMethod(Description = "Удалить пользователя")]
        public string AdminDeleteUser(string user, string password, int UserID)
        {
            ConnectionStateChecker();
            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }
            MySqlCommand com = new MySqlCommand(string.Format(@"Delete From UserData where ID ={0};",UserID), con);
            com.ExecuteNonQuery();
            return "User Deleted!";

        }

        [WebMethod(Description = "Удалить ключевое слово")]
        public string AdminDeleteKeyWord(string user, string password, int KeyWordID)
        {
            ConnectionStateChecker();
            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }
            MySqlCommand com = new MySqlCommand(string.Format(@"Delete From Keywords where ID ={0};", KeyWordID), con);
            com.ExecuteNonQuery();
            return "Keyword Deleted!";

        }

        [WebMethod(Description = "Добавление нового ключевого слова")]
        public string AdminAddNewKeyWord(string user, string password, string KeyWord, int PersonId)
        {
            ConnectionStateChecker();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }
            
            MySqlCommand com = new MySqlCommand(string.Format(@"INSERT INTO Keywords(Name, PersonId) value ('{0}', '{1}');", KeyWord.ToString(), PersonId), con);
            com.ExecuteNonQuery();
            return "Keyword Added!";
        }
        

        private void ConnectionStateChecker()
        {
            if (con.State != ConnectionState.Open) con.Open();
        }

        [WebMethod(Description ="Авторизация пользователей, если возращается 2 - администратор, 1 - пользователь, 0 - пользователь с такими данными не определен")]
        public int AutoriseUser(string user, string password)
        {
            ConnectionStateChecker();
            int count = 0;

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() != "0") count++;
                }
            }
            login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() != "0") count++;
                }
            }
            return count++;

        }

        [WebMethod(Description = "Добавление нового человека")]
        public string AdminAddNewPerson(string user, string password, string name)
        {
            ConnectionStateChecker();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }
            
            MySqlCommand com = new MySqlCommand(string.Format(@"INSERT INTO Persons (Name) VALUES ('{0}')", name), con);
            com.ExecuteNonQuery();
            return "New person added!";
        }

        [WebMethod(Description = "Удаление человека из обработки")]
        public string AdminDeletePerson(string user, string password, int PersonID)
        {
            ConnectionStateChecker();

            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}' AND IsAdmin = true;", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return "error";
                }
            }

            MySqlCommand com = new MySqlCommand(string.Format(@"delete from Persons Where ID ={0}", PersonID), con);
            com.ExecuteNonQuery();
            return "Person Deleted!";
        }

        [WebMethod(Description = "Получить информацию по отдельной персоне, по отдельному сайту, за определенный период времени, формат DataTable")]
        public DataTable GetSinglePersonInfoPerSiteForPeriod(string user, string password, int PersonID, int SiteId, DateTime date1, DateTime date2)
        {
            ConnectionStateChecker();


            MySqlCommand login = new MySqlCommand(string.Format(@"select count(*) from UserData where UserLogin = '{0}' AND UserPassword = '{1}';", user, password), con);
            using (var reader = login.ExecuteReader())
            {
                while (reader.Read())
                {
                    if (reader[0].ToString() == "0") return new DataTable();
                }
            }

            DataTable dt = new DataTable("Persons");
            MySqlCommand com = new MySqlCommand(string.Format(@"select P.Url,P.LastScanDate,R.Rank from PersonPageRank R,Pages P where R.PageID = P.ID and PersonID='{0}' and SiteID='{1}' and LastScanDate>='{2}' and LastScanDate<='{3}';", PersonID, SiteId, date1.ToString("yyyy-MM-dd"), date2.ToString("yyyy-MM-dd")), con);
            using (var reader = com.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    dt.Load(reader);
                }
            }
            return dt;
        }

    }

    
}
