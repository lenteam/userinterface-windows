﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI_win
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ColmenService.ColMenService_SOAPSoapClient client = new ColmenService.ColMenService_SOAPSoapClient("ColMenService_SOAPSoap");
        Dictionary<int, string> PersonList = new Dictionary<int, string>();
        Dictionary<int, string> SiteList = new Dictionary<int, string>();

        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.WidthAndHeight;
           
        }
        private void Autorisation(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            if (client.AutoriseUser(userBox.Text, passBox.Password) != 0)
            {
                PersonList.Clear();
                SiteList.Clear();
                tabControl.IsEnabled = true;
                dataGrid1.ItemsSource = client.GetAllPersonStatistic("user", "user").DefaultView;
                using (DataTable persons = client.GetPersons(userBox.Text, passBox.Password))
                {
                    if (persons.Rows.Count > 0) foreach (DataRow row in persons.Rows)
                        {
                            PersonList.Add(int.Parse(row[0].ToString()), row[1].ToString());
                        }
                }
                using (DataTable sites = client.GetSites(userBox.Text, passBox.Password))
                {
                    if (sites.Rows.Count > 0) foreach (DataRow row in sites.Rows)
                        {
                            SiteList.Add(int.Parse(row[0].ToString()), row[1].ToString());
                        }
                }
                personComboBox.ItemsSource = PersonList.Values.ToList();
                siteComboBox.ItemsSource = SiteList.Values.ToList();

            }
        (sender as Button).IsEnabled = true;
        }

        private void Kill_Autorisation(object sender, RoutedEventArgs e)
        {
            tabControl.IsEnabled = false;
        }

        private void GetDataButton_Click(object sender, RoutedEventArgs e)
        {

            if (dtpicker1.SelectedDate != null && dtpicker2.SelectedDate != null && personComboBox.SelectedItem!=null && siteComboBox.SelectedItem!=null )
                dataGrid2.ItemsSource = client.GetSinglePersonInfoPerSiteForPeriod(userBox.Text, passBox.Password, PersonList.Where(x => x.Value == personComboBox.SelectedItem.ToString()).FirstOrDefault().Key,
                    SiteList.Where(x => x.Value == siteComboBox.SelectedItem.ToString()).FirstOrDefault().Key, (DateTime)dtpicker1.SelectedDate, (DateTime)dtpicker2.SelectedDate).DefaultView;


        }
    }
}
