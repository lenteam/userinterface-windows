﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            ColMenModel model = new ColMenModel();

            model.UserRoles.AddRange(new List<UserRole>
            {
                new UserRole { RoleName="Admin" },
                new UserRole { RoleName="User" }
            });
            model.SaveChanges();

            model.Users.AddRange(new User[]
            {
                new User { UserLogin="aschapov", UserPassword="123456", UserRole=model.UserRoles.FirstOrDefault(x=>x.RoleId==1)}
            });
            model.SaveChanges();

            List<Site> sites = new List<Site>();
            string[] names = {"news.mail.ru", "news.yandex.ru", "news.rambler.ru", "rbc.ru", "rian.ru", "infox.ru", "lenta.ru", "kp.ru", "gazeta.ru", "utro.ru", "newsru.com",
                "vesti.ru", "aif.ru", "gzt.ru", "pravda.ru", "svpressa.ru", "rosbalt.ru", "vedomosti.ru", "inosmi.ru", "echo.msk.ru", "kommersant.ru", "regnum.ru", "izvestia.ru",
                "eg.ru", "rbcdaily.ru", "vz.ru", "rg.ru", "ng.ru", "rb.ru"};
            foreach (var name in names)
            {
                sites.Add(new Site { Name = name });
            }

            model.Sites.AddRange(sites);
            model.SaveChanges();
            
            model.Persons.AddRange(new List<Person> {
                new Person {Name="Путин"},
                new Person {Name="Медведев"},
                new Person {Name="Собчак"},
                new Person {Name="Жириновский"}
            });
            model.SaveChanges();

            List<Person> persons =  new List<Person>();
            StreamReader reader = new StreamReader("Names.txt");
            while(!reader.EndOfStream)
            {

                string[] namess = reader.ReadLine().Split(';');
                if(namess[0].Length>2)
                {
                    persons.Add(new Person {Name=namess[0] });
                }
            }
            model.Persons.AddRange(persons);
            model.SaveChanges();



            Console.WriteLine("All Done!");

            Console.ReadKey();
        }
    }
}
